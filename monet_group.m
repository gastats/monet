function [finalgrphs lambdas varargout] = monet_group(DataMatrix,varargin)
	% This demo estimates group level Markov networks given 
	% a time-series x regions x subject datamatrix. By default
	% this function centers and scales the time-series and does not
	% pre-whiten the time series before estimating Markov networks.
	%
	% Arguments
	% 	DataMatrix - a matrix containing time-series x regions x subject 
	% 	ngroupx	- (optional) a vector containing the number of subjects in 
	% 	each group. Assumes one group with the total number of subjects by default. 
	% 
	% 	Optional Arguments
	% 	whiten - Turns on prewhitening filter if 1. Default off. 
	% 	fullPath  - Provides graphs over a range of penalty parameters in addition to the 
	%		final selected graph. Default off. 
	% 	dispFig - Displays adjacency matrix of final graphs. Default off. 
	%		If fullPath is chosen then dispFig option provides a movie of graph plots.
	% 	beta - Model tuning parameter that specifies how much edge instability is tolerated. Default value is .1. Takes values between [0,.5].
	% 
	% 	Outputs
	% 	finalgrphs - is a cell that contains group-level graphs of subjects, assuming that 
	%	lambdas	- is a cell that contains the group-level regularization parameter chosen
	%
	% 	Optional Outputs
	% 	pathgrphs - is a cell containing group-level graphs of subjects across all lambda values
	%		Is produced only when fullPath is 1. 
	% 	lambdarange - is a cell containing the range of regularization values (lambda) used to produce 
	%		pathgrphs. Is produced only when fullPath is 1. 
	%
	% Example: 
	% Suppose 'subjectdata' contains data for 81 subjects and 2 group graphs 
	% based on the ngroup1 = 41 and ngroup2 = 40 are desired, run the following — 
	% 	monet_group(subjectdata,[41 40])	 
	% 
	% finalgrphs{1} is a region x region connectivity matrix that indexes into group 1. 
	% This corresponsds to a sparse inverse correlation or partial correlation matrix.
	% 		imagesc(finalgrphs{1})
	% Visualize a binary matrix as 
	% 		imagesc(finalgrphs{1}~=0) 
	% lambas{1} is a single numerical regularization parameter chosen for group 1. 
	%
	
	
	switch nargin
	case 3
		whiten = varargin{2};
		fullPath = 0;
		dispFig = 0;
		sel_beta = 0;						
	case 4
		whiten = varargin{2};
		fullPath = varargin{3};	
		dispFig = 0;
		sel_beta = 0;			
	case 5
		whiten = varargin{2};
		fullPath = varargin{3};						
		dispFig = varargin{4};
		sel_beta = 0;
	case 6
		whiten = varargin{2};
		fullPath = varargin{3};						
		dispFig = varargin{4};
		sel_beta = varargin{5};										
	otherwise
		whiten = 0;
		fullPath = 0;
		dispFig = 0; 
		sel_beta = 0;		
	end
	
	
	%% IF NO WHITENING
	if(~whiten)
		% for cc=1:size(DataMatrix,3)
		% 	DataMatrix(:,:,cc) = zscore(DataMatrix(:,:,cc)); % n x p matrix. 	
		% end
		for cc=1:size(DataMatrix,3)
			D = squeeze(DataMatrix(:,:,cc));
			tempD = bsxfun(@minus, D, mean(D,1));
			tempD = bsxfun(@rdivide, tempD, sqrt(var(tempD))); % Used for Real Data Application on May 28
			DataMatrix(:,:,cc) = tempD;
		end
	else
	%% IF WHITENING
		DataMatrix = preWhiten(DataMatrix);
	end	
	
	%% RESHAPE INTO GROUPS
	n = size(DataMatrix,1); p = size(DataMatrix,2); 
	if(nargin>=2)
		ngroups = length(varargin{1});
		s = varargin{1};
		if(sum(s)~=size(DataMatrix,3))
			error('Check input ngroupx. Total does not match number of subjects in DataMatrix')
		end
	else
		ngroups = 1;
		s = size(DataMatrix,3);	
		%ngroups = 2;
		%s = [floor(size(DataMatrix,3)/2) floor(size(DataMatrix,3)/2)]
	end
	
	tempdm = cell(1,ngroups); 
	for nn=1:ngroups
		if(nn==1)
			tempdm{1} = reshape(DataMatrix(:,:,1:s(nn)),[n*s(nn) p]); 			
		else
			tempdm{nn} = reshape(DataMatrix(:,:,sum(s(1:(nn-1)))+1:sum(s(1:nn))),[n*s(nn) p]); 
		end
	end
	
	
	%% CALL MoNeT TO ESTIMATE NETWORKS
	%% Since groups do not necessarily have a balanced group of subjects; the time series length
	%% can vary across groups. Hence store group data matrices in a cell format like tempdm. 
		finalgrphs = cell(1,ngroups);
		lambdas = cell(1,ngroups); 
		pathgrphs = cell(1,ngroups);
		lambdarange = cell(1,ngroups);
		monetobj = cell(1,ngroups);
		
		for nn=1:ngroups
			disp(sprintf('Estimating Group %d ... \n',nn))
			grphobj = monet(tempdm{nn});
			if(sel_beta~=0) % set selection parameter
				grphobj.setSelParams('beta',sel_beta);
			end
			grphobj.monetSelect();
			if(~fullPath)
				finalgrphs{nn} = grphobj.selResults.grphs{1};
				lambdas{nn} = grphobj.selResults.lambdas{1};
			else
				finalgrphs{nn} = grphobj.selResults.grphs{1};			
				lambdas{nn} = grphobj.selResults.lambdas{1};
				stability{nn} = grphobj.selResults.stability{1};
				pathgrphs{nn} = grphobj.selResults.grphpath{1};
				lambdarange{nn} = grphobj.lrange;

			end
			monetobj{nn} = grphobj;
		end
	
	if(dispFig)
		%if(~fullPath)
			figure(1)
			for nn=1:ngroups
				subplot(1,ngroups,nn); imagesc(finalgrphs{nn}~=0); axis square
				title(sprintf('Group Graph %d',nn));
			end
		% else
		% 	figure(1)
		% 	for mm=1:size(pathgrphs{1},3)
		% 		for nn=1:ngroups
		% 			subplot(1,ngroups,nn); imagesc(squeeze(pathgrphs{nn}(:,:,mm))~=0); axis square
		% 			title(sprintf('Group Graph %d',nn));
		% 		end
		% 		pause(.5)
		% 	end
		% end
	end
	
	if(fullPath)
		varargout = cell(1,3);
		varargout{1} = pathgrphs;
		varargout{2} = lambdarange;	
		varargout{3} = monetobj;
	end
	
	