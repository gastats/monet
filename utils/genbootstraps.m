function [submat] = genbootstraps(N,n,b,varargin)
	
	seed = varargin{1};
	
	scurr = rng;
	rng(seed);
	
	submat = zeros(N,n);
	for i = 1:N
		submat(i,:) = randsample(n,n,1);
	end
	
	rng(scurr);
end