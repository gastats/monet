function [submat] = gensubsamples(N,n,b,varargin)
	
	seed = varargin{1};	
	%y = randsample(population,k)
	
	scurr = rng;
	rng(seed);	
	submat = zeros(N,b);
	for i = 1:N
		submat(i,:) = randsample(n,b);
	end
	rng(scurr);
end