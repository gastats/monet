function [finalgrphs lambdas varargout] = monet_subjects(DataMatrix,varargin)
	% This demo estimates subject level Markov networks given 
	% a time-series x regions x subject datamatrix. All subjects are
	% estimated independently from one another at this time.  
	% By default this function centers and scales the time-series and does not
	% pre-whiten the time series before estimating Markov networks.
	%
	% Arguments
	% 	DataMatrix - time-series x regions x subject 
	% 
	% 	Optional Arguments
	% 	whiten - Turns on prewhitening filter if 1. Default off. 
	% 	fullPath  - Provides graphs over a range of penalty parameters in addition to the 
	%		final selected graph. Default off. 
	% 	dispFig - Displays adjacency matrix of final graphs. Default off. 
	%		If fullPath is chosen then dispFig option provides a movie of graph plots.	
	% 	beta - Model tuning parameter that specifies how much edge instability is tolerated. Default value is .1. Takes values between [0,.5]. Not yet operational.
	%
	% 	Outputs
	% 	finalgrphs - is a cell that contains individual-level graphs of subjects,
	%	lambdas - is a cell containing the final chosen regularization 
	%		parameter for each subject
	%
	% 	Optional Outputs
	% 	pathgrphs - is a cell containing individual-level inverse covariance graphs of subjects across all lambda values.
	%		Is produced only when fullPath is 1. 
	% 	lambdarange - is a cell containing the range of regularization values (lambda) used to produce 
	%		pathgrphs. Is produced only when fullPath is 1. 
	% 
	% Example: 
	% Suppose 'subjectdata' contains data for 81 subjects and all subject 
	% graphs are desired, run the following — 
	% 	monet_subjects(subjectdata)	 
	% 
	% finalgrphs{1} is a region x region connectivity matrix that indexes into subject 1. 
	% This corresponds to a sparse inverse correlation or partial correlation matrix.
	% 		imagesc(finalgrphs{1})
	% Visualize a binary matrix as 
	% 		imagesc(finalgrphs{1}~=0) 
	% lambas{1} is a single numerical regularization parameter chosen for subject 1. 

	switch nargin
	case 3
		whiten = varargin{1};
		fullPath = 0;
		dispFig = 0;
		sel_beta = 0;						
	case 4
		whiten = varargin{1};
		fullPath = varargin{2};	
		dispFig = 0;
		sel_beta = 0;			
	case 5
		whiten = varargin{1};
		fullPath = varargin{2};						
		dispFig = varargin{3};
		sel_beta = 0;
	case 6
		whiten = varargin{1};
		fullPath = varargin{2};						
		dispFig = varargin{3};
		sel_beta = varargin{4};										
	otherwise
		whiten = 0;
		fullPath = 0;
		dispFig = 0; 
		sel_beta = 0;		
	end


	%% IF NO WHITENING
	if(~whiten)
		% for cc=1:size(DataMatrix,3)
		% 	DataMatrix(:,:,cc) = zscore(DataMatrix(:,:,cc)); % n x p matrix. 	
		% end
		for cc=1:size(DataMatrix,3)
			D = squeeze(DataMatrix(:,:,cc));
			tempD = bsxfun(@minus, D, mean(D,1));
			tempD = bsxfun(@rdivide, tempD, sqrt(var(tempD))); % Used for Real Data Application on May 28
			DataMatrix(:,:,cc) = tempD;
		end
	else
	%% IF WHITENING
		DataMatrix = preWhiten(DataMatrix);
	end
	
	n = size(DataMatrix,1); p = size(DataMatrix,2); s = size(DataMatrix,3);  	
		
	%% CALL MoNeT TO ESTIMATE NETWORKS
 
	disp(sprintf('Estimating Subject Graphs ... \n'));
	grphobj = monet(DataMatrix);
	if(sel_beta~=0) % set selection parameter
		grphobj.setSelParams('beta',sel_beta);
	end
	grphobj.monetSelect();
	if(~fullPath)
		finalgrphs = cell(1,s);
		lambdas = cell(1,s);
		for cc=1:s
			finalgrphs{cc} = grphobj.selResults.grphs{cc};
			lambdas{cc} = grphobj.selResults.lambdas{cc};
		end
	else
		finalgrphs = cell(1,s);
		lambdas = cell(1,s);
		pathgrphs = cell(1,s);
		lambdarange = cell(1,s);
		for cc=1:s
			finalgrphs{cc} = grphobj.selResults.grphs{cc};			
			lambdas{cc} = grphobj.selResults.lambdas{cc};
			stability{cc} = grphobj.selResults.stability{cc};			
			% Use grphpath instead of theta_grphs to see biased/shrunken adjacency matrices over the full path
			pathgrphs{cc} = grphobj.selResults.grphpath{cc}; 
			lambdarange{cc} = grphobj.lrange;
		end
	end
	
	if(dispFig)
		%if(~fullPath)
			figure(2)
			for cc=1:s
				subplot(1,s,cc); imagesc(finalgrphs{cc}~=0); axis square
				title(sprintf('Subject Graph %d',cc));
			end
		% else
		% 	figure(2)
		% 	for mm=1:size(pathgrphs{1},3)
		% 		for cc=1:s
		% 			subplot(1,s,cc); imagesc(squeeze(pathgrphs{cc}(:,:,mm))~=0); axis square
		% 			title(sprintf('Subject Graph %d',cc));
		% 		end
		% 		pause(.5)
		% 	end
		% end
	end
	
	
	if(fullPath)
		varargout = cell(1,3);
		switch nargout		
		case 4
			varargout{1} = pathgrphs;			
			varargout{2} = lambdarange;	
		case 5
			varargout{1} = pathgrphs;			
			varargout{2} = lambdarange;	
			varargout{3} = grphobj;	
		otherwise
			varargout{1} = pathgrphs;
		end
	else
		varargout = cell(1,1);
	end
	
	
	
