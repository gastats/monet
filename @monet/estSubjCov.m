function obj = estSubjCov(obj,varargin)
% Estimate covariance or correlation matrices for each subjects in dm. 
% By default computes correlation matrices using @corr
%
% Arguments: 
% optional
% 	myfun 	@corr or @cov or any function that equivalent to correlation.
%
% Output: 
% 		
% 	dmCov 	A single cell structure. This contains a single covariance
% 	matrix based on myfun of size roi x roi x subject using the datamatrix
%	obj.dm  as input.  

%%%% Remove, following statements used for Testing%%%%
		% obj.estParams.resamplesOn = 1
	
	if(obj.estParams.resamplesOn)
		dmCov = cell(1,obj.estParams.nresamples);
		parfor(ii=1:obj.estParams.nresamples,2)
			tempmat = obj.estParams.resamples(ii,:);
			dmCov{ii} = estSubjCovHelper(obj,squeeze(obj.dm(tempmat,:,:)),varargin{:});							
		end
		obj.dmCov = dmCov;
		clear dmCov;
	else
		dmCov = cell(1); 
		dmCov{1} = estSubjCovHelper(obj,obj.dm,varargin{:});
		obj.dmCov = dmCov;
		clear dmCov
	end
	
end

function dmCov = estSubjCovHelper(obj,dm,varargin)

	dmCov = zeros(obj.p,obj.p,obj.s);
	%tempCov = dmCov{1};
	if(nargin>=3)
		myfun = varargin{1}; 
	else
		myfun = @corr; 
	end

	try
		if(matlabpool('size')==0)
			parforOn = 0; 
		else
			parforOn = 1;
			matlabpool local;
		end
	catch
		parforOn = 0;
	end
	
	if(parforOn)
		parfor cc=1:obj.s
			dmCov(:,:,cc) = feval(myfun,squeeze(dm(:,:,cc)));	
		end
		%dmCov{1} = tempCov;
	else
		for cc=1:obj.s
			dmCov(:,:,cc) = feval(myfun,squeeze(dm(:,:,cc)));
		end
	end	
		
end