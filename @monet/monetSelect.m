function obj = monetSelect(obj,varargin)
        
        selResults = {};
        
        %-- Check dmCov and update minLambda and maxLambda
        %%%%%%% NOTE: Ideally this should be separate for each subject
        % maxLambda = max(max(abs(triu(obj.dmCov{1}(:,:,1),1)))); 
        % obj.updateRegularization('maxLambda',maxLambda);
        
        %-- Ensure that estimation occurs from sparsest to densest graphs (required for stars)
        obj.updateRegularization('lrange',fliplr(obj.lrange));
        
        %-- Depending on obj.selMethod; generate resampled subject covariances 
        resmethods= cell(1,2); resmethods{1} = 'stars'; resmethods{2}='ssel'; 
        if(sum(strcmp(obj.selMethod,resmethods)))
                if(obj.estParams.resamplesOn==0)
                        %disp(sprintf('Resamples On \n'));
                        obj.updateEstParams('resamplesOn',1);
                end
        else
                if(obj.estParams.resamplesOn==1)
                        %disp(sprintf('Resamples Off \n'));
                        obj.updateEstParams('resamplesOn',0);
                end
        end             
        %%% NOTE: test that estSubjCov is automatically called and dmCov updated
                
                        
        %--Call monetEstimate() to generate full path, with 'pathmode' set to 1. 
		if(isempty(obj.selParams));
        	obj.setSelParams(); %initialize only if selParams is empty. 
		end
		     
        if(obj.estParams.pathmode==0)
                disp(sprintf('Pathmode On \n'));
                obj.updateEstParams('pathmode',1);
        end             
        obj.estResults = obj.monetEstimate(); 
        
        %-- Selection logic based on 'stars' or 'ssel'  
        if(strcmp(obj.selMethod,'stars'))
                
                grphs = obj.estResults.grphs;
                theta_ustat = zeros(obj.p,obj.p,obj.nlambda);           
                try
                        %if(size(grphs{1}(:,:,:,cc))~=size(theta_ustat))                
                        theta_ustat+(abs(grphs{1}(:,:,:,1))>0);
                catch
                        warning('estResults.grphs is incompatible with full path u-statistics')                         
                end
                
                grphs2 = cell(1,obj.s); % ultimately setSelParams()
				grphs3 = cell(1,obj.s);
                psi_grphs = cell(1,obj.s);
                theta_grphs = cell(1,obj.s);
                Dhat = cell(1,obj.s);
                lambdas = cell(1,obj.s);
				stb_grphs = cell(1,obj.s);
				
        
                disp(sprintf('Selecting Optimal Penalty ...\n'))
                for cc=1:obj.s
                        theta_ustat = zeros(obj.p,obj.p,obj.nlambda); 
						disp(sprintf('Graph No. %d, Percentage Remaining \n',cc));
						%disp(sprintf('... \n'));
		                %percount(max((cc-1),0)*(obj.nlambda)+2,(obj.s)*(obj.nlambda));      												                  
                        parfor ii=1:length(grphs)
                                % if(mod(ii,11)==0)        
                                %         percount(max((cc-1),0)*length(grphs)+ii+1,(obj.s)*length(grphs));  
                                % end  
								percount(ii+1,length(grphs)+1);								   
                                theta_ustat = theta_ustat+(abs(squeeze(grphs{ii}(:,:,:,cc)))>0);                        
                        end
                        for jj=1:obj.nlambda
                                theta_grphs{cc}(:,:,jj) = triu(squeeze(theta_ustat(:,:,jj)),1)/obj.estParams.nresamples;
                        end
                        psi_grphs{cc} = 2*((theta_ustat/obj.estParams.nresamples).*(1-(theta_ustat/obj.estParams.nresamples)));
                        
                        Dhat{cc} = zeros(1,length(obj.nlambda));
                        for jj=1:obj.nlambda
                                Dhat{cc}(jj) = sum(sum(triu(squeeze(psi_grphs{cc}(:,:,jj)),1)))/nchoosek(obj.p,2); 
                                % Total instability over all edges
                        end
                        
                        %-- Choose final lambda parameter
                        D_bl(1) = Dhat{cc}(1);
                        D_bl(2:obj.nlambda) = max(Dhat{cc}(1:end-1),Dhat{cc}(2:end));
                        
                        [finalL ind] = find((D_bl <= obj.selParams.beta)); %% Does this change anything ?
                        [finalL2 ind2] = max(D_bl(ind));
                        
                        if(ind2>0)
                                lambdas{cc} = obj.lrange(ind2); % NOTE lrange for each subject needed
                        else
                                lambdas{cc} = obj.lrange(obj.nlambda); % NOTE lrange for each subject needed
                                warning('Total Instability > Tolerance beta. Try increasing beta');
                        end 
                        
                        %-- Set 'pathmode' to 0. Find the final graph estimates and chosen lambda value                         
                        if(strcmp(obj.estMethod,'QUIC'))
                                tmpCov = feval(@corr,(squeeze(obj.dm(:,:,cc)))); % NOTE parameter corrtype needed in obj.
                                old_l = obj.lrange(ind2);
                                maxLambda = max(max(abs(triu(tmpCov,1)))); 
                                obj.updateRegularization('maxLambda',maxLambda);
                                obj.updateRegularization('lrange',fliplr(obj.lrange));
                                                                                                
                                [theta w optP timeP iterP dGapP] = ... 
                                        QUIC('path', tmpCov, 1.0, obj.lrange, 1e-6, 0, 1000);
                        else
                                error('Estimation method in obj.estMethod is not supported. Please use obj.estMethod="QUIC" '); 
                        end     
                        
                        [finalL3 ind3] = min(abs(obj.lrange-old_l));
                        %disp(sprintf('Old Lambda: %f, New Lambda %f',old_l,obj.lrange(ind3))); 
                        lambdas{cc} = obj.lrange(ind3)
						Lweight = squeeze(theta(:,:,ind3)==0)*1000;
                        [theta_refit w optP timeP iterP dGapP] = ... 
                                QUIC('default', tmpCov, Lweight, 1e-6, 0, 1000); 
						% convert to partial correlation matrix using Theta = W'Gamma W; 
						W = diag(theta_refit(find(eye(length(theta_refit)))));	                
                        grphs2{cc} = sqrt(inv(W))'*theta_refit*sqrt(inv(W));
						grphs3{cc} = theta;
						stb_grphs{cc} = theta_grphs{cc}(:,:,ind2); % ind2 equiv. to ind3 over stability path                        
                        
                end
                disp(sprintf('Penalty Parameters Selected. \n'))
                
                selResults.psi_grphs = psi_grphs;
                selResults.theta_grphs = theta_grphs;
                selResults.Dhat = Dhat;
                selResults.lambdas = lambdas;
                selResults.grphs = grphs2;
				selResults.stability = stb_grphs;
				selResults.grphpath = grphs3;

                
        else
                error('Only "stars" option is currently supported for obj.selMethod');
        end
        
        obj.selResults = selResults;            
        
end     
