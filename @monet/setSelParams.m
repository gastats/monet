function obj = setSelParams(obj,varargin)
	%%% Reset minlambda and maxlambda
	selParams = {};		
	
	if(nargin<=1)
		selParams.a_w = .5; % perturbation parameter for stability selection
		if(obj.n>=4*obj.p)
			selParams.beta = .1; % stars algorithm beta parameter, tolerates upto beta edge variance
		else
			selParams.beta = .1;
		end 	
	elseif(nargin==3)
		selParams.a_w = .5;
		selParams = setfield(selParams,varargin{1},varargin{2});
	end	
	obj.selParams = selParams;
	
	if(strcmp(obj.selMethod,'ssel'))
		% modify obj.lrange
		obj.updateRegularization('minLambda',.2); 
		obj.updateRegularization('maxLambda',.8);
				
		obj.updateEstParams('sresamples',ceil(obj.n/2));
		
	elseif(strcmp(obj.selMethod,'stars'))		
		% modify obj.estParams.sresamples
		if((obj.n>144) & (obj.p>obj.n))
			tempvar = ceil(10*sqrt(obj.n));
		elseif(obj.n<144)
			tempvar = max(ceil(.8*obj.n),80);
		else
			tempvar = max(ceil(.51*obj.n),80);
		end
		obj.updateEstParams('sresamples',tempvar);							
	end
	
end