function obj = updateRegularization(obj,varargin)

	if(nargin ~= 3)
		error('updateRegularization must have one field and one value to update')
	end
	
	updateField = varargin{1};
	updateVal   = varargin{2};
	
	checkUpdate(updateField,updateVal);
	
	if(strcmp(updateField,'nlambda'))
		% Make the user specified update
		obj = setfield(obj,updateField,updateVal);
		
		% Compute update changes for lrange
		lrangeUpdate = logspace(log10(obj.minLambda),log10(obj.maxLambda),obj.nlambda);
		obj = setfield(obj,'lrange',lrangeUpdate);
		
	elseif(strcmp(updateField,'maxLambda'))
		% Make the user specified update
		obj.maxLambda = updateVal;
		
		% Compute update changes for lrange
		lrangeUpdate = logspace(log10(obj.minLambda),log10(obj.maxLambda),obj.nlambda);
		obj = setfield(obj,'lrange',lrangeUpdate);
		
	elseif(strcmp(updateField,'minLambda'))
		% Make the user specified update
		obj.minLambda = updateVal;
		
		% Compute update changes for lrange
		lrangeUpdate = logspace(log10(obj.minLambda),log10(obj.maxLambda),obj.nlambda);
		obj = setfield(obj,'lrange',lrangeUpdate);
		
	elseif(strcmp(updateField,'lrange'))
		% Make the user specified update
		obj = setfield(obj,updateField,updateVal);
		
		% Update the parameters minLambda, maxLambda, and nlambda
		obj.minLambda = min(obj.lrange);
		obj.maxLambda = max(obj.lrange);
		obj = setfield(obj,'nlambda',length(obj.lrange));
			
	else
		error('Specified update field is not a valid field')
	end
		
end



function checkUpdate(updateField,updateVal)
	
	%% Check nlambda 
	if(strcmp(updateField,'nlambda'))
		if(isnumeric(updateVal) == 0)
			error('Update for nlambda must be numeric integer')
		end
		if(updateVal > 100)
			error('Update for nlambda must be less than 100')				
		end	
	end
	
	%% Check lrange
	if(strcmp(updateField,'lrange'))
		if(isnumeric(updateVal) == 0)
			error('Update for lrange must be a numeric vector')
		end
		if(((max(updateVal) > 1) || (min(updateVal) < 0)))
			error('Update for lrange should be a subset of the interval [0,1]')
		end
	end
	
	%% Check minLambda
	if(strcmp(updateField,'minLambda'))
		if(isnumeric(updateVal) == 0)
			error('Update for minLambda must be a numeric value')
		end
		if(((updateVal < 0) || (updateVal >= 1)) == 1)
			error('Update for minLambda must be in the interval [0,1)')
		end
	end
	
	%% Check maxLambda
	if(strcmp(updateField,'maxLambda'))
		if(isnumeric(updateVal) == 0)
			error('Update for maxLambda must be a numeric value')	
		end
		if(((updateVal <= 0 ) || (updateVal > 1)) == 1)
			error('Update for maxLambda must be in the interval (0,1]')
		end
	end	
	
end