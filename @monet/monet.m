classdef monet < handle
% Implements basic graph estimation and selection algorithms for Markov Networks
%
% Has the following properties
%
% Determine MONET input parameters
% 	getaccess, setaccess = public
% 	dm - datamatrix of timeseries x regions x subject
% 	estMethod - Currently supports  the 'QUIC' penalized maximum likelihood method
% 	selMethod - Currently supports  the 'stars' model selection method
% 	dmCov - a cell structure that contains the covariance matrix for every subject. 
% 		When resampling is turned on in estParams. This contains covariances for every
% 		resample. The default number of resamples is 100. 
% 	n	- number of time series samples
% 	p 	- number of regions
% 	s	- number of subjects
% 
% Regularization settings. Not advisable to change manually
% 	lambda - default lambda parameter for a single graph estimate
% 	nlambda - length of lrange
% 	lrange - range of regularization parameters for the full estimation path
% 	flagPlot - does nothing right now. debugging mode on or off
%
% Internal parameters for various functions
% 	getaccess = public, setaccess = private (cannot be changed manually)
% 	estParams
% 	selParams
% 	minLambda
% 	maxLambda
% 
% **Results**
% 	getaccess = public, setaccess = private (cannot be changed manually)
% 	estResults:
% 	A structure containing results from monetEstimate
%	estResults.grphs 	- For each subject presents graphs over the full 
%				regularization path in lrange
%
% 	selResults:
%	A structure containing results from monetSelect
% 	selResults.grphs 	- final graphs at a model selected regularization parameter 
% 	selResults.lambdas 	- model selected lambdas for each subject



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2013 Manjari Narayan & Jonathan Stewart
% All rights reserved
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * Neither the name of the <organization> nor the
%       names of its contributors may be used to endorse or promote products
%       derived from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY <copyright holder> AS IS'' AND ANY
% EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL <copyright holder> BE LIABLE FOR ANY
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


	properties(GetAccess = 'public', SetAccess = 'public')
	% public read access and public write access
		dm;  % Store time-series (n) x regions of interest (p) x subjects (s)
		estMethod; 
		selMethod; 
		flagPlot;
		dmCov; 
		
		n; 
		p; 
		s; 
		lambda; % regularization parameter that controls sparsity of estimates.
		nlambda;
		lrange;
	end

	properties(GetAccess = 'public', SetAccess = 'private')
		% public read access, but private write access.
			estParams = {}; 
			selParams = {};	
			minLambda = 1e-02; % needs to be set later
			maxLambda = 1; % needs to be set later
			estResults = {};
			selResults = {};
	end
		% 	
		%     properties(GetAccess = 'private', SetAccess = 'private')
		%         % private read and write access
		%     end

	methods

		function obj = monet(varargin)
		% Constructor for monet class	
		% function obj = monet(data,estMethod,selMethod,nlambda,lrange,flagPlot)	
		% Inputs
		% - dm: 		DataMatrix of time-series (n) x regions of interest (p) x subjects (s)
		% - estMethod: 	choose between penalized ML ('glasso') or neighborhood selection ('nsel')
		% 			   	'nsel' option is not yet implemented.
		% - selMethod: choose between StARS ('stars') or Stability Selection ('ssel')	
			narginchk(0,inf);
			nargoutchk(0,1);
					
			obj; 
			switch nargin
			case 1
				obj.dm = varargin{1};
				obj.estMethod = 'QUIC';
				obj.selMethod = 'stars';
				obj.nlambda = 30;				
				obj.lrange = logspace(log10(obj.minLambda),log10(obj.maxLambda),obj.nlambda);
				obj.flagPlot = 0; 		
				
			case 2
				obj.dm = varargin{1};
				if(ischar(varargin{2}))
					obj.estMethod = varargin{2}; 
				else 
					error('estMethod should be a string')
				end
				obj.selMethod = 'stars';
				obj.nlambda = 30;				
				obj.lrange = logspace(log10(obj.minLambda),log10(obj.maxLambda),obj.nlambda);
				obj.flagPlot = 0; 		
					
			case 3
				obj.dm = varargin{1};			
				if(ischar(varargin{2}))
					obj.estMethod = varargin{2}; 
				else 
					error('estMethod should be a string')
				end
				if(ischar(varargin{3}))
					obj.selMethod = varargin{3}; 
				else 
					error('selMethod should be a string')
				end	
				obj.nlambda = 30;
				obj.lrange = logspace(log10(obj.minLambda),log10(obj.maxLambda),obj.nlambda);
				obj.flagPlot = 0;		
				
			case 4
				obj.dm = varargin{1};
				if(ischar(varargin{2}))
					obj.estMethod = varargin{2};
				else
					error('estMethod should be a string')
				end
				if(ischar(varargin{3}))
					obj.selMethod = varargin{3};
				else 
					error('selMethod should be a string')
				end
				if(isnumeric(varargin{4}))
					if(varargin{4} > 100)
						error('nlambda cannot be larger than 100') % should there be a minimum too??
					end
					obj.nlambda = varargin{4};
				else
					error('nlambda should be a numeric integer')
				end
				obj.lrange = logspace(log10(obj.minLambda),log10(obj.maxLambda),obj.nlambda);
				obj.flagPlot = 0;
				
			case 5 
				obj.dm = varargin{1};
				if(ischar(varargin{2}))
					obj.estMethod = varargin{2};
				else 
					error('estMethod should be a string')
				end
				if(ischar(varargin{3}))
					obj.selMethod = varargin{3};
				else
					error('selMethod should be a string')
				end
				if(isnumeric(varargin{4}))
					if(varargin{4} > 100)
						error('nlambda cannot be larger than 100')
					end
					if(varargin{4} == length(varargin{5}))
						obj.nlambda = varargin{4};
					else
						obj.nlambda = length(varargin{5});
					end
				else
					error('nlambda should be a numeric integer')
				end
				if(isnumeric(varargin{5}))
					if((max(varargin{5}) > 1) || (min(varargin{5}) < 0))
						error('lrange should be a subset of [0,1]')
					end
					obj.lrange = varargin{5};
				else
					error('lrange must be a numeric vector')
				end
				obj.flagPlot = 0;
				
			case 6
				obj.dm = varargin{1};
				if(ischar(varargin{2}))
					obj.estMethod = varargin{2};
				else
					error('estMethod should be a string')
				end
				if(ischar(varargin{3}))
					obj.selMethod = varargin{3};
				else
					error('selMethod should be a string')
				end
				if(isnumeric(varargin{4}))
					if(varargin{4} > 100)
						error('nlambda cannot be larger than 100')
					end
					obj.nlambda = varargin{4}
				else
					error('nlambda should be a numeric integer')
				end
				if(isnumeric(varargin{5}))
					if((max(varargin{5}) > 1) || (min(varargin{5}) < 0))
						error('lrange should be a subset of [0,1]')
					end
					obj.lrange = varargin{5};
				else
					error('lrange must be a numeric vector')
				end
				if(isnumeric(varargin{6}))
					if((varargin{6} == 1) || (varargin{6} == 0))
						obj.flagPlot = varargin{6};
					else
						error('flagplot should be either 0 or 1')
					end
				else
					error('flagplot should be a numeric value 0 or 1')
				end
																	
			otherwise
				obj.dm = getfield(load('testdata'),'SampleD'); % load default dataset in testdata.mat
				obj.estMethod = 'QUIC';
				obj.selMethod = 'stars';
				obj.nlambda = 30;				
				obj.lrange = logspace(log10(obj.minLambda),log10(obj.maxLambda),obj.nlambda);
				obj.flagPlot = 0; 						
			end
			
			[obj.n obj.p obj.s]	= size(obj.dm);
			obj = obj.setEstParams();
			
			%%%%%% For now precompute covariance matrices %%%%%%%%%
			%---- Move to another function as appropriate-----%

			obj = obj.estSubjCov();
		
			%--------------------------------------------------%
		end
		
	end
	
end