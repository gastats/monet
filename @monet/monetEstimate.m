%function obj = monetEstimate(obj,varargin)
function estResults = monetEstimate(obj,varargin)

	dmCov = obj.dmCov;
	%dmCov = squeeze(obj.dmCov{:}(:,:,:)); % single subject, needs to be extended
	estResults = {};
	grphs = cell(1,length(dmCov));
	if(obj.estParams.pathmode)
		grphs{1} = zeros(obj.p,obj.p,obj.nlambda,obj.s);
		
	else	
		grphs{1} = zeros(obj.p,obj.p,obj.s);
	end
	estResults.grphs = grphs;
	
	%%%%% Future - Greate Graphs Object to embed in the Results structures
	% [estResults(1).lambda] = obj.lrange(1);
	% estResults(1:obj.nlambda).lambda %= obj.lrange(:)
	% size([estResults(1:obj.nlambda).grphs])
		
	if(strcmp(obj.estMethod,'QUIC'))	
		disp(sprintf('Estimating Graph(s) over all thresholds ... \n')); 						
		if(obj.estParams.resamplesOn) %%% Using Resamples
			for cc=1:obj.s
				disp(sprintf('Graph No. %d, Percentage Remaining \n',cc));
				maxLambda = max(max(abs(triu(obj.dmCov{1}(:,:,cc),1)))); 
				obj.updateRegularization('maxLambda',maxLambda);
				obj.updateRegularization('lrange',fliplr(obj.lrange));
				%disp(sprintf('... \n'));
				% if(cc==1)
				% 	(obj.s+1)*(obj.estParams.nresamples)
				% 	percount(2,(obj.s+1)*(obj.estParams.nresamples)); 	% print percent completed
				% 	
				% else
				% 	percount((max((cc-1),0)*obj.estParams.nresamples)+2,(obj.s+1)*obj.estParams.nresamples); 	% print percent completed
				% end
				if(obj.estParams.pathmode)	
																	
					parfor ii=1:obj.estParams.nresamples
						%--- Update LRange for each Resample----%
						% maxLambda = max(max(abs(triu(obj.dmCov{ii}(:,:,cc),1)))); 
						% obj.updateRegularization('maxLambda',maxLambda);
						% obj.updateRegularization('lrange',fliplr(obj.lrange));	
						%----%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%----%				
						% if(mod(ii,2)==0)
						% 	percount((max((cc-2),0)*obj.estParams.nresamples)+ii+1,...
						% 	(obj.s+1)*(obj.estParams.nresamples));
						% end	
						percount(ii+1,obj.estParams.nresamples+1);
						[theta w optP timeP iterP dGapP] = QUIC('path', dmCov{ii}(:,:,cc), 1.0, obj.lrange, 1e-6, 0, 1000);											
						grphs{ii}(:,:,:,cc) = theta;
					end					
				else
					
					parfor ii=1:obj.estParams.nresamples
						% if(mod(ii,2)==0)	
						% 	percount(((cc-1)*obj.estParams.nresamples)+ii+1,(obj.s+1)*(obj.estParams.nresamples));	
						% end	
						percount(ii+1,obj.estParams.nresamples+1);						
						[theta w optP timeP iterP dGapP] = QUIC('default', dmCov{ii}(:,:,cc), obj.lambda, 1e-6, 0, 1000);
						grphs{ii}(:,:,cc) = theta;						
					end
					%%% Implement 'default' or 'trace' optionality in the future
					% if(strcmp(obj.estparam,'default'))
					% 
					% elseif(strcmp(obj.estparam,'trace'))
					% 
					% end	
					
				end
				estResults.grphs = grphs;									
			end
		else %%% No Resamples
			estResults.grphs = grphs; % without parfor can update estResults directly
			for cc=1:obj.s	
				percount(cc+1,obj.s+1)							
				if(obj.estParams.pathmode)
					[theta w optP timeP iterP dGapP] = QUIC('path', dmCov{1}(:,:,cc), 1.0, obj.lrange, 1e-6, 0, 1000);						
					estResults.grphs{1}(:,:,:,cc) = theta;					
				else
					[theta w optP timeP iterP dGapP] = QUIC('default', dmCov{1}(:,:,cc), obj.lambda, 1e-6, 0, 1000);
					estResults.grphs{1}(:,:,cc) = theta;
					
					%%%% Implement 'default' or 'trace' optionality in the future
					% if(strcmp(obj.estparam,'default'))
					% 
					% elseif(strcmp(obj.estparam,'trace'))
					% 
					% end			
				end
			end			
		end
		disp(sprintf('\nSubject Graphs Estimated.\n'))
	else
		error('Estimation method in obj.estMethod is not supported. Please use obj.estMethod="QUIC" '); 
	end
	
end




% function obj = monetEstimateHelper(obj,varargin)
% 		
% end