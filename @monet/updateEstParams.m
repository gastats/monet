function obj = updateEstParams(obj,varargin)
	
	checkUpdate(obj,varargin{:})
	
	for ii = 1:2:(length(nargin))
		field = varargin{ii};
		if(isfield(obj.estParams,field))
			obj.estParams = setfield(obj.estParams,field,varargin{ii+1});
		else 
			error('Field specified to update is not a field in estParams')
		end
	end
	
	resamplesUpdateVal = feval(obj.estParams.resfun,...
							   obj.estParams.nresamples,...
							   obj.n,...
							   obj.estParams.sresamples,...
							   obj.estParams.seed);
							   
	obj.estParams = setfield(obj.estParams,'resamples',resamplesUpdateVal);
	obj = obj.estSubjCov();
	
end


function checkUpdate(obj,varargin)
	updates = varargin;
	
	numUpdates = 1:2:(length(updates));
	fieldUpdates = updates{numUpdates};
	
	%% Check seed
	if(any(strcmp(fieldUpdates,'seed')))
		fieldInd = find(strcmp(updates,'seed'));
		valInd   = fieldInd + 1;
		if(isnumeric(updates{valInd}) == 0)
			error('Update for seed must be numeric')
		end
	end
	
	%% Check pathmode
	if(any(strcmp(fieldUpdates,'pathmode')))
		fieldInd = find(strcmp(updates,'pathmode'));
		valInd   = fieldInd + 1;
		if(isnumeric(updates{valInd}) == 0)
			error('Update for pathmode must be numeric')
		end
		if(((updates{valInd} == 1) || (updates{valInd} == 0)) == 0)
			error('Update for pathmode must be 0 or 1')
		end
	end
	
	%% Check resamplesOn
	if(any(strcmp(fieldUpdates,'resamplesOn')))
		fieldInd = find(strcmp(updates,'resamplesOn'));
		valInd   = fieldInd + 1;
		if(isnumeric(updates{valInd} == 0))
			error('Update for resamplesOn must be numeric')
		end
		if(((updates{valInd} == 1) || (updates{valInd} == 0)) == 0)
			error('Update for resamplesOn must be 0 or 1')
		end
	end
	
	%% Check resfun
	if(any(strcmp(fieldUpdates,'resfun')))
		fieldInd = find(strcmp(updates,'resfun'));
		valInd   = fieldInd + 1;
		if(((isequal(updates{valInd},@genbootstraps)) || (isequal(updates{valInd},@gensubsamples))) == 0)
			error('Update for resfun does not specify a supported method')
		end
	end
	
	%% Check nresamples
	if(any(strcmp(fieldUpdates,'nresamples')))
		fieldInd = find(strcmp(updates,'nresamples'));
		valInd   = fieldInd + 1;
		if(isnumeric(updates{valInd}) == 0)
			error('Update for nresamples must be numeric')
		end
		if(updates{valInd} < 1)
			error('Update for nresamples must be at least 1')
		end
		if(updates{valInd} > 100)
			error('Update for nresamples cannot be greater than 100')
		end
		if(updates{valInd} < 50)
			warning('Update for nresamples not recommended to be less than 50')
		end
	end
	
	%% Check sresamples
	if(any(strcmp(fieldUpdates,'sresamples')))
		fieldInd = find(strcmp(updates,'sresamples'));
		valInd   = fieldInd + 1;
		if(isnumeric(updates{valInd}) == 0)
			error('Update for sresamples must be numeric')
		end
		if((updates{valInd}/obj.n) < 0.5)
			warning('Update for sresamples recommended to be at least 50% of data matrix n')
		end
		if(updates{valInd} > obj.n)
			error('Update for sresamples must be no more than data matrix n')
		end
	end
	
end