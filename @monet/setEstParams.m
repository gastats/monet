function obj = setEstParams(obj,varargin)
	
	%if(nargin==0)
		obj.estParams.seed = 1500; %notyet used
		obj.estParams.pathmode = 1;		 
		obj.estParams.resamplesOn = 0; 
		obj.estParams.resfun = @genbootstraps; % of 'subsample';
		obj.estParams.nresamples = 100; 
		obj.estParams.sresamples =  ceil(.8*obj.n); 
		% sresamples is only in play for subsamples. Should be reset for various methods
		obj.estParams.resamples = feval(obj.estParams.resfun,obj.estParams.nresamples,obj.n,obj.estParams.sresamples,obj.estParams.seed); 
	%end
			
end



% seed --- should be a number
% pathmode ---- should be 0 or 1
% resamplesOn --- should be 0 or 1
% resfun  ----- @genbootsraps or @gensubsample (check to have function in monet folder)
% nresamples  ----- a number minimum of 50, warning less than 50 not recomended, at least 1, no more than 100 

% sresamples --- should be minium of 50% of n, and a maximum of n, must be a number
% resamples ----- size of resamples should be size nresamples by sresamples for subsamples 
%           ------- for genbootsraps, ignore sresamples,   nresamples by obj.n
