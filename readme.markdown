# README 
## What is the **M**ark**o**v **Ne**twork **T**oolbox ? 

- MoNeT (*mow-net*) is a Matlab toolbox for functional connectivity analysis.  
- It can be used to estimate group and subject level Markov networks or undirected gaussian graphical models. 
- The current implementation combines a penalized maximum likelihood estimation procedure 
	with a resampling based model selection procedure to infer fully data driven functional connectivity estimates.
- An introduction to these techniques is available [here](https://bitbucket.org/gastats/monet/wiki/MONET).

## Citation
If you use this toolbox for data analysis please cite the toolbox and manual in the publication. 

- **Text**

	M. Narayan, J. Stewart, Steffie Tomson and G. I. Allen, Markov Network Toolbox (MoNeT) for Functional Connectivity, 2013.

- **Bibtex**

	@manual{Narayan:2013fu,
		Author = {Manjari Narayan and Jonathan Stewart and Steffie Tomson and Genevera I. Allen},
		Title = {Markov Network Toolbox (MoNeT) for Functional Connectivity },
		Year = {2013}}
	

## Installation

This toolbox runs in Matlab and requires the [QuIC](http://www.cs.utexas.edu/~sustik/QUIC/) package. QUIC requires a compiled mex file to run in Matlab. The toolbox and mex files are available at the [downloads](https://bitbucket.org/gastats/monet/downloads) page. This toolbox currently depends on 
the Statistics and Signal Processing toolboxes provided by Matlab. 

Download the compiled mex files (.mexw64,.mexmaci64,.mexa64) for [Windows](https://bitbucket.org/gastats/monet/downloads/QUIC.mexw64), [Mac](https://bitbucket.org/gastats/monet/downloads/QUIC.mexmaci64) or [Linux](https://bitbucket.org/gastats/monet/downloads/QUIC.mexa64). Once you add both QuIC and
the MONET packages to your Matlab path, you are all set to use the toolbox. 

**We've developed and tested the toolbox on Matlab 2012a versions. Compatibility with older versions of Matlab is not guaranteed, although we've found the toolbox to be compatible with Matlab 2011a on Linux. We highly recommend that you run this toolbox on a Linux machine with Matlab 2011a or higher.** 

## Getting Started
Once the toolbox has been installed, open Matlab.  Click File -> Set Path and select Add with Subfolders.  Add both the QuIC folder and the MONET folders to your matlab path. Now, run

    load('testdata.mat')
 
Optionally, if you have the parallel computing toolbox in Matlab, run 
	
	matlabpool open 4
	
to open 4 matlab workers on multi-core machines. Matlab 2012a and later permit upto 12 matlab workers on multi-core machines. This can substantially speedup toolbox performance.
	
You have just loaded a datamatrix SampleD into your workspace. SampleD is a synthetic testdata set containing 400 observations x 50 regions x 40 subjects. To estimate a group graph of the first 5 subjects, run 

    [finalgrphs lambdas pathgraphs lambdarange] = monet_group(SampleD(:,:,1:5), [5], 0, 1, 1);

Visualize resulting graph as a binary connectivity matrix using

	    imagesc(finalgrphs{1}~=0); 

To estimate two group level graphs, where the first 5 subjects are split into a group of 3 and a group of 2, run

	[finalgrphs lambdas pathgraphs lambdarange] = monet_group(SampleD(:,:,1:5),[3 2], 0 , 1, 1);	

To estimate individual subject graphs of the first 5 subjects, run 

	[finalgrphs lambdas pathgraphs lambdarange] = monet_subjects(SampleD(:,:,1:5), 0, 1, 1);
	
The second output argument *lambdas* provides the penalty parameter selected for each graph in *finalgrphs* by default.

The outputs pathgrphs and lambdarange are optional and provide graphs over a range of penalty parameters when the fullPath argument (optional) in monet_group() or monet_subjects() is set to 1. For example if you run

	[finalgrphs lambdas pathgrphs lambdarange] = monet_group(SampleD(:,:,1:5), [5], 0, 1 ,1);

each pathgrphs{i} is a matrix of 50 region x 50 region x 30 parameters, by default. This provides graph estimates over 30 lambda penalty parameters and can be visualized using
	    
		imagesc(squeeze(pathgrphs{i}(:,:,l))~=0); axis square; 

There are three optional input parameters *whiten*, *fullPath* and *dispFig* for both monet_subjects and monet_group. These arguments are logical flags that turn on the pre-whitening filter, provide graph estimates over the regularization path and display adjacency matrices of each graph, respectively. By default if no arguments are passed, all are set 'off' to 0.


-----

## License
Markov Network Toolbox (MONET) for Functional Connectivity

Copyright (C) 2013 Manjari Narayan & Jonathan Stewart

All rights reserved

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <copyright holder> AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <copyright holder> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#### Contact
We recommend that you read all the documentation first before contacting us. You are more likely to receive timely response, if you indicate a very specific concern that has not already been addressed in the documentation. 

Please email monetteam.mat at google mail, if you have any questions. 
